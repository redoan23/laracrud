@extends('welcome')
@section('layout')
@php
    $value= explode(',',$student->hobbies);
@endphp
    <div>
        <a href="{{ url('/student') }}">Back</a>
    </div>
    <form method="POST" action="{{ action('App\Http\Controllers\StudentController@update', $student) }}">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="name" value="{{ $student->name }}">
            </div>
        </div>
        <div class="form-group row">
            <label for="email" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" name="email" value="{{ $student->email }}">
            </div>
        </div>
        <div class="row">
            <p>Gender</p>
            <div class="col-sm-2">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="radio" value="male" @if ($student->gender == 'male')
                    checked
                    @endif>
                    <label class="form-check-label">
                        Male
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="radio" value="female" @if ($student->gender == 'female')
                    checked
                    @endif>
                    <label class="form-check-label">
                        Female
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group col-md-4">
            <label for="class">Class</label>
            <select name="class" class="form-control">
                <option disabled>Choose...</option>
                <option @if ($student->class == '6')
                    selected
                    @endif>6</option>
                <option @if ($student->class == '7')
                    selected
                    @endif>7</option>
                <option @if ($student->class == '8')
                    selected
                    @endif>8</option>
                <option @if ($student->class == '9')
                    selected
                    @endif>9</option>
                <option @if ($student->class == '10')
                    selected
                    @endif>10</option>
            </select>
        </div>
        <div class="row">
            <p>Hobbies</p>
            <div class="col-sm-2">
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="hobbies[]" value="MobileGaming" @if (in_array('MobileGaming',$value))
                            checked
                        @endif>Mobile
                        Gaming</label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="hobbies[]" value="Travelling" @if (in_array('Travelling',$value))
                        checked
                    @endif>
                        Travelling
                    </label> 
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="hobbies[]" value="Playing" @if (in_array('Playing',$value))
                    checked
                @endif>
                        Playing
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="hobbies[]" value="Reading" @if (in_array('Reading',$value))
                    checked
                @endif>
                        Reading
                    </label>
                </div>
            </div>
        </div>
        <button class="btn btn-primary" type="submit" style="color: aliceblue">Submit</button>
    </form>
@endsection
