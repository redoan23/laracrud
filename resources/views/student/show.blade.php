@extends('welcome')
@section('layout')
@php
    $checkb= explode(',',$student->hobbies);
@endphp
<table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">Id</th>
            <th scope="col">Name</th>
            <th scope="col">Class</th>
            <th scope="col">Gender</th>
            <th scope="col">Hobbies</th>
            <th scope="col">Email</th>
          </tr>
        </thead>
        <tbody>
        <tr>
        <th scope="row">{{ $student->id }}</th>
            <td>{{ $student->name }}</td>
            <td>{{ $student->class }}</td>
            <td>{{ $student->gender }}</td>
            <td>{{ implode(',', $checkb) }}</td>
            <td>{{ $student->email }}</td>
          </tr>
        </tbody>
      </table>
      @endsection
