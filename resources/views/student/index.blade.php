@extends('welcome')
@section('layout')
    <div class="row">
        <h1 style="color: cornflowerblue">All Data</h1>
    </div>
    <div class="text-center"><a href="{{ route('student.create') }}">
            <h3>Insert New Student</h3>
        </a></div>
        @if (Session::has('success'))
        <div class="alert alert-success">
            <p>{{ Session::get('seccess') }}</p>
        @endif
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Name</th>
                <th scope="col">Class</th>
                <th scope="col-span-2">Action</th>

            </tr>
        </thead>
        <tbody>
            @foreach ($student as $item)
                <tr>
                    <th scope="row">{{ $item->id }}</th>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->class }}</td>
                    <td><a href="{{ action('App\Http\Controllers\StudentController@edit', $item->id) }}"
                      class="btn btn-warning">Edit</a></td>
              <td><a class="btn btn-info"
                      href="{{ action('App\Http\Controllers\StudentController@show', $item->id) }}">Show</a></td>
              <td>
                  <form action="{{ action('App\Http\Controllers\StudentController@destroy', $item->id) }}"
                      method="POST">
                      {{ csrf_field() }}
                      <input name="_method" type="hidden" value="DELETE">
                      <button class="btn btn-danger" onclick="return confirm('Do you want to delete this?')"
                          type="submit">Delete</button>
                  </form>
              </td>
                  </tr>
            @endforeach
            
        </tbody>
    </table>
@endsection
