<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index()
    {
        $student=Student::all();
        return view('student.index', compact('student'));
    }

    public function create()
    {
        return view('student.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
        ]);
        $stu= new Student();
        $stu->name= $request->get('name');
        $stu->email= $request->get('email');
        $stu->gender= $request->get('radio');
        $stu->class= $request->get('class');
        $checkb= implode(',',$request->get('hobbies'));
        $stu->hobbies= $checkb;
        $stu->save(); 
        return redirect('/student')->with('success','New Student Added');
    }

    public function show(Student $student)
    {
        return view('student.show', compact('student'));
    }

    public function edit(Student $student)
    {
        return view('student.edit',compact('student'));
    }

 
    public function update(Request $request, Student $student)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
        ]);
        $student->name= $request->get('name');
        $student->email= $request->get('email');
        $student->gender= $request->get('radio');
        $student->class= $request->get('class');
        $checkb= implode(',',$request->get('hobbies'));
        $student->hobbies= $checkb;
        $student->save(); 
        return redirect('/student')->with('success','Student Updeted');
    }

    public function destroy(Student $student)
    {
        $student->delete();
        return redirect('/student')->with('success','Student Deleted');
    }
}
