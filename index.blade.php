@extends('blogs.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Check all Blogs</h2>
            </div>
            <div class="text-center">
                <a class="btn btn-success" href="{{ route('blogs.create') }}"> Create new blogs</a>
            </div>
        </div>
    </div>

    <div class="container">
        <br/>
        @if (Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div><br/>
        @endif
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>CoinName</th>
                    <th>CoinPrice</th>
                    <th colspan="2">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($blogs as $form)
                    <tr>
                        <td>{{ $form->id }}</td>
                       
                        <td>{{ $form->coinname }}</td>
                        <td>{{ $form['coinprice'] }}</td>
                        <td><a href="{{ action('App\Http\Controllers\BlogController@edit', $form['id']) }}"
                                class="btn btn-warning">Edit</a></td>
                        <td><a class="btn btn-info"
                                href="{{ action('App\Http\Controllers\BlogController@show', $form['id']) }}">Show</a></td>
                        <td>
                            <form action="{{ action('App\Http\Controllers\BlogController@destroy', $form['id']) }}"
                                method="post">
                                {{ csrf_field() }}
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="btn btn-danger" onclick="return confirm('Do you want to delete this?')"
                                    type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $blogs->links() }}
    </div>

@endsection
